import 'package:agenda_contatos/helpers/ui/home_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Agenda de contatos",
    home: HomePage(),
    debugShowCheckedModeBanner: false,
  ));
}
