import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:loja_virtual/tiles/category_tile.dart';

class ProductsTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<QuerySnapshot>(
      future: Firestore.instance.collection("produtos").getDocuments(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(child: CircularProgressIndicator());
        } else {
          var divideTile = ListTile.divideTiles(
            tiles: snapshot.data.documents.map((document) {
              return CategoryTile(document);
            }).toList(),
            color: Colors.grey[500],
          ).toList();

          return ListView(
            children: divideTile,
          );
        }
      },
    );
  }
}
