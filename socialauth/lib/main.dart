import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:socialauth/screens/home_screen.dart';

void main() {
  runApp(MaterialApp(
    title: "Social Auth",
    debugShowCheckedModeBanner: false,
    home: HomeScreen(),
  ));
}
