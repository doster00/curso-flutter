import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:socialauth/screens/home_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        padding: EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Icon(
              Icons.memory,
              size: 200.0,
              color: Colors.redAccent,
            ),
            SizedBox(height: 22.0),
            TextField(
              controller: null,
              decoration: InputDecoration(
                labelText: "E-mail",
                labelStyle: TextStyle(color: Colors.redAccent),
                fillColor: Colors.red,
                border: OutlineInputBorder(
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(25.0),
                  ),
                ),
                prefixIcon: const Icon(
                  Icons.mail,
                  color: Colors.redAccent,
                ),
              ),
              style: TextStyle(
                color: Colors.redAccent,
                fontSize: 18.0,
              ),
            ),
            Divider(),
            TextField(
              obscureText: true,
              decoration: InputDecoration(
                labelText: "Senha",
                labelStyle: TextStyle(color: Colors.redAccent),
                border: OutlineInputBorder(
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(25.0),
                  ),
                ),
                prefixIcon: const Icon(
                  Icons.lock,
                  color: Colors.redAccent,
                ),
              ),
              style: TextStyle(
                color: Colors.redAccent,
                fontSize: 18.0,
              ),
            ),
            SizedBox(height: 22.0),
            RaisedButton(
              padding: EdgeInsets.symmetric(vertical: 12.0),
              onPressed: () {},
              child: Text(
                "Entrar",
                style: TextStyle(fontSize: 18.0),
              ),
              color: Colors.redAccent,
              textColor: Colors.black,
            ),
            SizedBox(height: 22.0),
            GoogleSignInButton(
              onPressed: () {
                Center(child: CircularProgressIndicator());
                _loginWithGoogle().then((user) {
                  print(user);
                  if (user != null) {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => HomeScreen(),
                      ),
                    );
                  }
                });
              },
            ),
            FacebookSignInButton(
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }
}

Future<FirebaseUser> _loginWithGoogle() async {
  FirebaseUser firebaseUser;
  GoogleSignIn googleSignIn = GoogleSignIn();
  FirebaseAuth auth = FirebaseAuth.instance;
  GoogleSignInAccount user = googleSignIn.currentUser;

  if (user == null) {
    user = await googleSignIn.signInSilently();
  }

  if (user == null) {
    user = await googleSignIn.signIn();
  }

  if (firebaseUser == null) {
    firebaseUser = await auth.currentUser();
  }

  if (await auth.currentUser() == null) {
    GoogleSignInAuthentication credentials =
        await googleSignIn.currentUser.authentication;
    firebaseUser = await auth.signInWithCredential(
        GoogleAuthProvider.getCredential(
            idToken: credentials.idToken,
            accessToken: credentials.accessToken));
  }

  return firebaseUser;
}
