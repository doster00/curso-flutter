import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:youtube_search/blocs/favorite_bloc.dart';
import 'package:youtube_search/blocs/videos_bloc.dart';
import 'package:youtube_search/delegates/data_search.dart';
import 'package:youtube_search/models/video.dart';
import 'package:youtube_search/screens/favorites.dart';
import 'package:youtube_search/widgets/video_tile.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Container(
          height: 25,
          child: Image.asset("images/logo.png"),
        ),
        elevation: 0,
        backgroundColor: Colors.black,
        actions: <Widget>[
          Align(
            alignment: Alignment.center,
            child: StreamBuilder<Map<String, Video>>(
              initialData: {},
              stream: BlocProvider.of<FavoriteBloc>(context).outFav,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Text("${snapshot.data.length}");
                } else {
                  return Container();
                }
              },
            ),
          ),
          IconButton(
            icon: Icon(Icons.star),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => Favorites(),
                ),
              );
            },
          ),
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () async {
              String result =
                  await showSearch(context: context, delegate: DataSearch());
              if (result != null && result.isNotEmpty) {
                BlocProvider.of<VideosBloc>(context).inSearch.add(result);
              }
              print(result);
            },
          ),
        ],
      ),
      body: StreamBuilder(
        initialData: [],
        stream: BlocProvider.of<VideosBloc>(context).outVideos,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Container();
          }

          return ListView.builder(
            itemBuilder: (context, index) {
              if (index < snapshot.data.length) {
                return VideoTile(snapshot.data[index]);
              } else if (index > 1) {
                BlocProvider.of<VideosBloc>(context).inSearch.add(null);
                return Container(
                  height: 40,
                  width: 40,
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
                  ),
                );
              } else {
                return Container();
              }
            },
            itemCount: snapshot.data.length + 1,
          );
        },
      ),
    );
  }
}
